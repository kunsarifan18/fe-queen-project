import { createRouter, createWebHistory } from "vue-router";
import { useUserStore } from "../stores/user";

const routes = [
  {
    path: "/",
    name: "index",
    component: () => import("../views/Index.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: () => import("../views/Dashboard.vue"),
  },
  {
    path: "/menu",
    name: "menu.index",
    component: () => import("../views/menu/Index.vue"),
  },
  {
    path: "/menu/create",
    name: "menu.create",
    component: () => import("../views/menu/Create.vue"),
  },
  {
    path: "/menu/edit/:id",
    name: "menu.edit",
    component: () => import("../views/menu/Edit.vue"),
  },
  {
    path: "/laporan",
    name: "laporan.index",
    component: () => import("../views/laporan/Index.vue"),
  },
  {
    path: "/transaction",
    name: "transaction.index",
    component: () => import("../views/transaction/Index.vue"),
  },
  {
    path: "/transaction/create",
    name: "transaction.create",
    component: () => import("../views/transaction/Create.vue"),
  },
  {
    path: "/transaction/edit/:id",
    name: "transaction.edit",
    component: () => import("../views/transaction/Edit.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const userStore = useUserStore();
  const isAuth = localStorage.getItem("loggedIn");

  if (to.name !== "login" && !isAuth) next({ name: "login" });
  if (to.name === "login" && isAuth) next({ name: "dashboard" });
  else next();
});

export default router;
