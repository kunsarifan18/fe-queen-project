import { defineStore } from "pinia";
import api from "../services/api";
import token from "../services/token";
// import router from "../router";

export const useLaporanStore = defineStore({
  id: "laporan",
  state: () => ({
    listLaporan: [],
  }),
  getters: {
    getLaporanPerAuthor: (state) => {
      return (id) => state.listLaporan.filter((laporan) => post.id === id);
    },
  },
  actions: {
    async fetchLaporans() {
      this.listLaporan = [];
      await api
        .get("/laporan")
        .then((result) => {
          this.listLaporan = result.data;
          console.log(this.listLaporan);
        })
        .catch((err) => {
          console.log(err.response);
        });
    },
  },
});
