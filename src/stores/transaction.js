import { defineStore } from "pinia";
import api from "../services/api";
import token from "../services/token";
import router from "../router";

export const useTransactionStore = defineStore({
  id: "transaction",
  state: () => ({
    menu_order: [],
    order: {},
    transaction: {
      customer_name: "",
      driver_name: "",
      type: "",
      paid_amount: 0,
      user_id: "",
      order: [],
    },
    type: [],
    types: [
      { text: "offline", value: "1" },
      { text: "shopefood", value: "2" },
      { text: "grabfood", value: "3" },
      { text: "gofood", value: "4" },
    ],
    total: "",
    cash: "",
    returnCash: "",
    validation: [],
  }),
  getters: {
    menuTransaction: (state) => state.menu_order,
    grantTotal: (state) => state.transaction.paid_amount,
    getReturnCash: (state) => state.cash - state.transaction.paid_amount,
  },
  actions: {
    postTransaction() {
      api
        .post("/transaction", this.transaction)
        .then((response) => {
          console.log(response);
        })
        .catch((err) => {
          this.validation = err.response.data;
        });
    },
  },
});
