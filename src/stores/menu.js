import { defineStore } from "pinia";
import api from "../services/api";
import token from "../services/token";
import router from "../router";

export const useMenuStore = defineStore({
  id: "menu",
  state: () => ({

    list_menu: [],
    id: "",
    getListId: {},
    form: {
      name: "",
      price: "",
      category: "",
      desc: "",
    },

    categories: [
      { text: "Makanan Utama", value: "1" },
      { text: "Makanan Ringan", value: "2" },
    ],
    validation: [],
  }),
  getters: {
    getId: (state) => state.id,
    byId: (state) => state.getListId,
    getCategory: (state) => state.category,
    // menuTransaction: (state) => state.menu_transaction,
  },
  actions: {
    async getMenu() {
      await api
        .get("/menu")
        .then((result) => {

          this.list_menu = result.data.data;
          console.log(this.list_menu);
        })
        .catch((err) => {
          console.log(err.response);
        });
    },
    postMenu() {
      api
        .post("/menu", this.form)
        .then((response) => {
          console.log(response);
          router.push({ name: "menu.index" });
        })
        .catch((err) => {
          this.validation = err.response.data;
        });
    },
    getEditMenu() {
      api
        .get(`/menu/${this.id}`)
        .then((result) => {
          this.form.name = result.data.data.name;
          this.form.price = result.data.data.price;
          this.form.category = result.data.data.category;
          this.form.desc = result.data.data.desc;
        })
        .catch((err) => {
          console.log(err.response.data);
        });
    },
    editMenu() {
      api
        .put(`/menu/${this.id}`, this.form)
        .then((response) => {
          console.log(response);
          router.push({ name: "menu.index" });
        })
        .catch((err) => {
          this.validation = err.response.data;
        });
    },
    destroyMenu() {
      api
        .delete(`/menu/${this.id}`)
        .then((response) => {

          this.list_menu.splice(this.list_menu, 1);
        })
        .catch((err) => {
          console.log(err.response.data);
        });
    },
  },
});
