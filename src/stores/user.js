import { defineStore } from "pinia";
import api from "../services/api";
import token from "../services/token";
import router from "../router";
// import { useRouter } from "vue-router";
// const router = useRouter();

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    user: {
      email: "",
      password: "",
    },
    users: {},
    token: localStorage.getItem("token"),
    message: "",
    error: "",
    loggedIn: localStorage.getItem("loggedIn"),
    loginFailed: false,

    id: "",
  }),
  getters: {
    isLoggedIn: (state) => state.loggedIn,
  },
  actions: {
    async login() {
      await token.get().then((response) => {
        console.log(response);

        api.post("login", this.user).then((res) => {
          this.token = res.data.token;

          if (res.data.success) {
            //set localStorage
            localStorage.setItem("loggedIn", "true");

            //set localStorage Token
            localStorage.setItem("token", res.data.token);

            //change state
            this.loggedIn = true;

            //redirect dashboard
            router.push({ name: "dashboard" });
          } else {
            //set state login failed
            this.loginFailed = true;
          }
        });
      });
    },

    async getUser() {
      await api
        .get("/user", { headers: { Authorization: "Bearer " + this.token } })
        .then((response) => {
          //console.log(response.data.name)
          this.users = response.data;
          console.log(this.users);
        })
        .catch((error) => {
          console.log(error.response);
        });
    },

    logout() {
      localStorage.removeItem("loggedIn");
      localStorage.removeItem("token");
      this.loggedIn = false;

      router.push({ name: "login" });
    },
  },
});
