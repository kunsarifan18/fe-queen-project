import axios from "axios";

const instance = axios.create({
  // baseURL: "https://extrylinux.herokuapp.com/api",
  baseURL: "http://localhost:8000/sanctum/csrf-cookie",
  headers: {
    "Content-Type": "application/json",
  },
});

export default instance;
