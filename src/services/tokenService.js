class TokenService {
        getLocalRefreshToken() {
                const user = JSON.parse(localStorage.getItem("user"));
                return user?.refreshToken
        }

        getLocalAccessToken() {
                const user = JSON.parse(localStorage.getItem("user"));
                return user?.accessToken;
        }

        updateLocalAccessToken() {
                let user = JSON.parse(localStorage.getItem("user"));
                user.accessToken = token;
                localStorage.setItem("user", JSON.stringify(user));
        }

        getUser() {
                // return JSON.parse(localStorage.getItem("user"));
                const user = JSON.parse(localStorage.getItem("user"));               
                return user?.username;
        }

        setUser(user) {
                console.log(JSON.stringify(user));
                localStorage.setItem("user", JSON.stringify(user));
        }

        setPassword(password) {
                console.log(JSON.stringify(password));
                localStorage.setItem("password", JSON.stringify(password));
        }

        removeUser() {
                localStorage.removeItem("user");
                localStorage.removeItem("username");
        }

        removePassword() {
                localStorage.removeItem("password");
        }

}

export default new TokenService();