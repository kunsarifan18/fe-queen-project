import axios from "axios";
import token from "../services/token";

// const token = document.head.querySelector('meta[name="csrf-token"]');

const instance = axios.create({
  // baseURL: "https://extrylinux.herokuapp.com/api",
  baseURL: "http://localhost:8000/api",
  headers: {
    "Content-Type": "application/json",
    // withCredentials: true,
    "X-Requested-With": "XMLHttpRequest",
    // "X-CSRF-TOKEN": document
    //   .querySelector('meta[name="csrf-token"]')
    //   .getAttribute("content"),
  },
});

export default instance;
