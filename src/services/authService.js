import api from './api';
import tokenService from './tokenService';
import cryptoJs from 'crypto-js';

class AuthService {
  login({ name, password }) {
    return api
    .post("/login", {
      name,
      password
    })
    .then((response) => {
      const encrypted = CryptoJS.AES.encrypt(
        password,
        "password"
      ).toString();
      localStorage.setItem("password", encrypted);
      localStorage.setItem("name", name);
        
      if (!response.data.accessToken) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
      }
      
      if (response.data.accessToken) {
        tokenService.setUser(response.data);
      }

      return response.data;
    });
  }

  logout() {
    tokenService.removeUser();
    tokenService.removePassword();
  }

  register({ name, email, password }) {
    return api.post("/register", {
      name,
      email, 
      password
    });
  }
}

export default new AuthService();
